//
//  LFMArtist.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LFMObject.h"
#import "LFMCountry.h"

@interface LFMArtist : LFMObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *listeners;
@property (nonatomic, strong) LFMCountry *country;

@end
