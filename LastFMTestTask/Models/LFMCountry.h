//
//  LFMCountry.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

#import "LFMObject.h"

@interface LFMCountry : LFMObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *iconFileName;

- (NSDictionary *)dictionaryRepresentation;
- (UIImage *)icon;
@end
