//
//  LFMCountry.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMCountry.h"

static NSString const *LFMCountryName = @"country";
static NSString const *LFMCountryDisplayName = @"name";
static NSString const *LFMCountryIconFileName = @"icon";

@implementation LFMCountry

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[LFMCountryName] = self.name;
    
    return params;
}

- (void)fillWithDictionary:(NSDictionary *)jsonObject
{
    self.name = jsonObject[LFMCountryName];
    self.displayName = jsonObject[LFMCountryDisplayName];
    self.iconFileName = jsonObject[LFMCountryIconFileName];
}

- (UIImage *)icon
{
    return [UIImage imageNamed:self.iconFileName];
}
@end
