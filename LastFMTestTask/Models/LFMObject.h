//
//  LFMObject.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString const *LFMMBIDKey;
extern NSString const *LFMImageUrlKey;
extern NSString const *LFMImageSizeKey;
extern
NSString const *LFMImageMedium;

@interface LFMObject : NSObject

@property (nonatomic, strong) NSString *mbid;

+ (instancetype)createWithDictionary:(NSDictionary *)jsonObject;

- (void)fillWithDictionary:(NSDictionary *)jsonObject;

@end
