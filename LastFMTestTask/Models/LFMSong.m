//
//  LFMSong.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMSong.h"

static NSString const *LFMSongNameKey = @"name";
static NSString const *LFMSongDurationKey = @"duration";

@implementation LFMSong

- (void)fillWithDictionary:(NSDictionary *)jsonObject
{
    self.title = jsonObject[LFMSongNameKey];
    self.duration = [jsonObject[LFMSongDurationKey] integerValue];
}

- (NSString *)getDurationString
{
    long minutes = (long)(self.duration / 60);
    long seconds = self.duration - minutes * 60;
    return [NSString stringWithFormat:@"%ld:%02ld", minutes, seconds];
}
@end
