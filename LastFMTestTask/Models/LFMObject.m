//
//  LFMObject.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMObject.h"

NSString const *LFMMBIDKey =@"mbid";
NSString const *LFMImageUrlKey = @"#text";
NSString const *LFMImageSizeKey = @"size";
NSString const *LFMImageMedium = @"medium";

@implementation LFMObject

+ (instancetype)createWithDictionary:(NSDictionary *)jsonObject
{
    LFMObject *object = [[self alloc] init];
    
    [object fillWithDictionary:jsonObject];
    
    return object;
}

- (void)fillWithDictionary:(NSDictionary *)jsonObject
{
    [NSException raise:NSInternalInconsistencyException
                format:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)];
}

- (NSDictionary *)dictionaryRepresentation
{
    @throw [NSException exceptionWithName:NSInternalInconsistencyException
                                   reason:[NSString stringWithFormat:@"You must override %@ in a subclass", NSStringFromSelector(_cmd)]
                                 userInfo:nil];
}
@end
