//
//  LFMArtist.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMArtist.h"

static NSString const *LFMArtistNameKey = @"name";
static NSString const *LFMArtistUrlKey = @"url";
static NSString const *LFMArtistImageKey = @"image";
static NSString const *LFMArtistListenersKey = @"listeners";

@implementation LFMArtist

- (void)fillWithDictionary:(NSDictionary *)jsonObject
{
    self.name = jsonObject[LFMArtistNameKey];
    self.url = jsonObject[LFMArtistUrlKey];
    self.listeners = jsonObject[LFMArtistListenersKey];
    self.mbid = jsonObject[LFMMBIDKey];
    
    for (NSDictionary *image in jsonObject[LFMArtistImageKey]) {
        
        if ([LFMImageMedium isEqualToString:image[LFMImageSizeKey]]) {
            self.imageUrl = image[LFMImageUrlKey];
        }
    }
}

- (NSDictionary *)dictionaryRepresentation
{
    NSMutableDictionary *json = [NSMutableDictionary new];
    
    json[@"artist"] = self.name;
    
    return json;
}
@end
