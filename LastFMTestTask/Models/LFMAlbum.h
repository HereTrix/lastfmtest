//
//  LFMAlbum.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LFMObject.h"
#import "LFMArtist.h"

@class UIImage;

@interface LFMAlbum : LFMObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *listeners;
@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) LFMArtist *artist;

- (void)getCover:(void(^)(UIImage *))completion;
@end
