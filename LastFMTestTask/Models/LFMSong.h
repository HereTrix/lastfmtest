//
//  LFMSong.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMObject.h"

#import "LFMAlbum.h"

@interface LFMSong : LFMObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic) NSInteger duration;

@property (nonatomic, strong) LFMAlbum *album;

- (NSString *)getDurationString;
@end
