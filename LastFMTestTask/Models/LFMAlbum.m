//
//  LFMAlbum.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMAlbum.h"

#import "LFMAPIManager.h"

static NSString const *LFMAlbumTitleKey = @"name";
static NSString const *LFMAlbumListenersKey = @"playcount";
static NSString const *LFMAlbumUrlKey = @"url";

static NSString const *LFMAlbumImageKey = @"image";

@interface LFMAlbum()

@property (atomic, strong) UIImage *coverImage;
@end

@implementation LFMAlbum

- (void)fillWithDictionary:(NSDictionary *)jsonObject
{
    self.title = jsonObject[LFMAlbumTitleKey];
    self.listeners = jsonObject[LFMAlbumListenersKey];
    self.mbid = jsonObject[LFMMBIDKey];
    
    for (NSDictionary *image in jsonObject[LFMAlbumImageKey]) {
        
        if ([LFMImageMedium isEqualToString:image[LFMImageSizeKey]]) {
            self.imageUrl = image[LFMImageUrlKey];
        }
    }
}

- (void)getCover:(void(^)(UIImage *))completion
{
    if (self.coverImage) {
        completion(self.coverImage);
    } else {
        [[LFMAPIManager sharedInstance] getImage:self.imageUrl
                                      completion:^(UIImage *image) {
                                          if (image) {
                                              self.coverImage = image;
                                          }
                                          completion(self.coverImage);
                                      }];
    }
}
@end
