//
//  LMFAlbumCollectionViewCell.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LFMAlbum.h"

@interface LFMAlbumCollectionViewCell : UICollectionViewCell

@property (nonatomic, strong) LFMAlbum *album;

- (void)setCoverImage:(UIImage *)image;
@end
