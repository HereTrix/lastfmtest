//
//  ArtistTableViewCell.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMArtistTableViewCell.h"

#import "LFMArtist.h"

@interface LFMArtistTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *artistNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *artistListenersLabel;
@property (nonatomic, weak) IBOutlet UIImageView *artistCoverView;
@property (nonatomic, weak) IBOutlet UILabel *orderNumberLabel;

@end

@implementation LFMArtistTableViewCell

@synthesize artist = _artist;

- (void)setArtist:(LFMArtist *)artist
{
    _artist = artist;
    [self fillWithArtist:artist];
}

- (LFMArtist *)artist
{
    return _artist;
}

- (void)fillWithArtist:(LFMArtist *)artist
{
    self.artistNameLabel.text = artist.name;
    self.artistListenersLabel.text = [NSString stringWithFormat:@"%@ %@", artist.listeners, NSLocalizedString(@"ListenersAdditionString", nil)];
}

- (void)setCoverImage:(UIImage *)image
{
    self.artistCoverView.image = image;
}

- (void)setOrder:(NSInteger)index
{
    self.orderNumberLabel.text = [NSString stringWithFormat:@"%ld", (long)index];
}
@end
