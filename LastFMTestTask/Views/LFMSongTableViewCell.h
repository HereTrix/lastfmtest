//
//  LFMSongTableViewCell.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFMSong;

@interface LFMSongTableViewCell : UITableViewCell

@property (nonatomic, strong) LFMSong *song;

- (void)setOrder:(NSInteger)index;
@end
