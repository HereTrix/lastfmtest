//
//  LFMCountryCell.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMCountryCell.h"

#import "LFMCountry.h"

@interface LFMCountryCell()

@property (nonatomic, weak) IBOutlet UIImageView *countryFlagView;
@property (nonatomic, weak) IBOutlet UILabel *countryNameLabel;
@end

@implementation LFMCountryCell
@synthesize country = _country;

- (void)setCountry:(LFMCountry *)country
{
    _country = country;
    [self fillWithCountry:country];
}

- (void)fillWithCountry:(LFMCountry *)country {
    self.countryFlagView.image = country.icon;
    self.countryNameLabel.text = country.displayName;
}
@end
