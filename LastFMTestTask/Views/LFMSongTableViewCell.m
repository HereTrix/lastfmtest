//
//  LFMSongTableViewCell.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMSongTableViewCell.h"

#import "LFMSong.h"

@interface LFMSongTableViewCell()

@property (nonatomic, weak) IBOutlet UILabel *orderLabel;
@property (nonatomic, weak) IBOutlet UILabel *songTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *songDurationLabel;
@end

@implementation LFMSongTableViewCell
@synthesize song = _song;

- (void)setSong:(LFMSong *)song
{
    _song = song;
    [self fillWithSong:song];
}

- (void)fillWithSong:(LFMSong *)song
{
    self.songTitleLabel.text = song.title;
    self.songDurationLabel.text = [song getDurationString];
}

- (void)setOrder:(NSInteger)index
{
    self.orderLabel.text = [NSString stringWithFormat:@"%ld", index];
}
@end
