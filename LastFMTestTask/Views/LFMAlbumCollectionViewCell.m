//
//  LMFAlbumCollectionViewCell.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMAlbumCollectionViewCell.h"

@interface LFMAlbumCollectionViewCell()

@property (nonatomic, weak) IBOutlet UIImageView *albumCoverView;
@property (nonatomic, weak) IBOutlet UILabel *albumTitleLabel;
@property (nonatomic, weak) IBOutlet UILabel *listenersLabel;
@end

@implementation LFMAlbumCollectionViewCell

@synthesize album = _album;

- (void)setAlbum:(LFMAlbum *)album
{
    _album = album;
    [self fillWithAlbum:album];
}

- (void)fillWithAlbum:(LFMAlbum *)album
{
    self.albumTitleLabel.text = album.title;
    self.listenersLabel.text = [NSString stringWithFormat:@"%@ %@", album.listeners, NSLocalizedString(@"ListenersAdditionString", nil)];
}

- (void)setCoverImage:(UIImage *)image
{
    self.albumCoverView.image = image;
}
@end
