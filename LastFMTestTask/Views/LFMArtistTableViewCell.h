//
//  ArtistTableViewCell.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFMArtist;

@interface LFMArtistTableViewCell : UITableViewCell

@property (nonatomic, strong) LFMArtist *artist;

- (void)setCoverImage:(UIImage *)image;
- (void)setOrder:(NSInteger)index;
@end
