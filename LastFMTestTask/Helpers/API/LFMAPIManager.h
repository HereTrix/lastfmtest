//
//  APIManager.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^LFMAPICompletionBlock)(NSArray *loadedObjects, NSError *error);

@class LFMObject;
@class LFMCountry;
@class LFMArtist;
@class LFMAlbum;
@class UIImage;

@interface LFMAPIManager : NSObject

+ (instancetype)sharedInstance;

- (void)getTopArtistsForCountry:(LFMCountry *)country
                         atPage:(NSInteger)page
                     completion:(LFMAPICompletionBlock)completionBlock;

- (void)getTopAlbumsForArtist:(LFMArtist *)artist
                   completion:(LFMAPICompletionBlock)completionBlock;

- (void)getTracksForAlbum:(LFMAlbum *)album
               completion:(LFMAPICompletionBlock)completionBlock;

- (void)getImage:(NSString *)imageUrl completion:(void(^)(UIImage *image))completion;
@end