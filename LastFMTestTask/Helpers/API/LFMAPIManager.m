//
//  APIManager.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMAPIManager.h"

#import <UIKit/UIKit.h>

#import "NSDictionary+URLStringEncoding.h"

#import "LFMCountry.h"
#import "LFMArtist.h"
#import "LFMAlbum.h"
#import "LFMTrack.h"
#import "LFMSong.h"

static NSString const *LFMAPIKey = @"9c0cbfb76c4b7e2b3e4e559d8d0ff13c";

static NSString const *LastFMBaseURL = @"http://ws.audioscrobbler.com/2.0/";

static NSString const *LFMMethodNameKey = @"method";

static NSString const *LFMGeoTopArtistsMethodName = @"geo.getTopArtists";
static NSString const *LFMArtistTopAlbumsMethodName = @"artist.getTopAlbums";
static NSString const *LFMAlbumGetInfoMethodName = @"album.getInfo";

@interface LFMAPIManager()

@property (nonatomic, strong) NSURLSession *session;
@property (nonatomic, strong) NSURLSession *downloadSession;

@end

@implementation LFMAPIManager

+ (instancetype)sharedInstance
{
    static LFMAPIManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
        configuration.URLCache = [NSURLCache sharedURLCache];
        configuration.requestCachePolicy = NSURLRequestReturnCacheDataElseLoad;
        self.downloadSession = [NSURLSession sessionWithConfiguration:configuration];
        
        self.session = [NSURLSession sharedSession];
    }
    return self;
}


#pragma mark - Request processing
- (void)getTopArtistsForCountry:(LFMCountry *)country
                         atPage:(NSInteger)page
                     completion:(LFMAPICompletionBlock)completionBlock
{
    
    NSMutableDictionary *params = [self defaultRequestParams];
    
    params[LFMMethodNameKey] = LFMGeoTopArtistsMethodName;
    params[@"page"] = @(page);
    
    [params addEntriesFromDictionary:[country dictionaryRepresentation]];
    
    [self startTaskWithParams:params completion:^(id receivedObjects, NSError *error) {
        
        if (error) {
            completionBlock(nil, error);
        } else {
            
            NSDictionary *topArtists = receivedObjects[@"topartists"];
            NSArray *artistObjects = topArtists[@"artist"];
            NSMutableArray *artists = [NSMutableArray new];
            
            for (NSDictionary *jsonObject in artistObjects) {
                LFMArtist *artist = [LFMArtist createWithDictionary:jsonObject];
                [artists addObject:artist];
            }
            completionBlock(artists,nil);
        }
    }];
    
}

- (void)getTopAlbumsForArtist:(LFMArtist *)artist
                   completion:(LFMAPICompletionBlock)completionBlock
{
    NSMutableDictionary *params = [self defaultRequestParams];
    
    params[@"method"] = LFMArtistTopAlbumsMethodName;
    
    params[LFMMBIDKey] = artist.mbid;
    
    [self startTaskWithParams:params completion:^(id receivedObjects, NSError *error) {
        if (error) {
            completionBlock(nil, error);
        } else {
            
            NSDictionary *topalbums = receivedObjects[@"topalbums"];
            NSArray *albumObjects = topalbums[@"album"];
            
            NSMutableArray *albums = [NSMutableArray new];
            
            for (NSDictionary *jsonObject in albumObjects) {
                LFMAlbum *album = [LFMAlbum createWithDictionary:jsonObject];
                album.artist = artist;
                [albums addObject:album];
            }
            
            completionBlock(albums,nil);
        }
    }];
}

- (void)getTracksForAlbum:(LFMAlbum *)album
               completion:(LFMAPICompletionBlock)completionBlock
{
    NSMutableDictionary *params = [self defaultRequestParams];
    
    params[@"method"] = LFMAlbumGetInfoMethodName;
    
    params[@"artist"] = album.artist.name;
    params[@"album"] = album.title;
    
    [self startTaskWithParams:params completion:^(id receivedObjects, NSError *error) {
        if (error) {
            completionBlock(nil, error);
        } else {
            
            NSDictionary *albumObject = receivedObjects[@"album"];
            NSDictionary *tracks = albumObject[@"tracks"];
            NSArray *trackObjects = tracks[@"track"];
            
            NSMutableArray *songs = [NSMutableArray new];
            for (NSDictionary *jsonObject in trackObjects) {
                LFMSong *song = [LFMSong createWithDictionary:jsonObject];
                song.album = album;
                [songs addObject:song];
            }
            completionBlock(songs,nil);
        }
    }];
}

- (void)getImage:(NSString *)imageUrl completion:(void(^)(UIImage *image))completion;
{
    NSURL *url = [NSURL URLWithString:imageUrl];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSURLSessionDownloadTask *task =[self.downloadSession downloadTaskWithRequest:request
                                                            completionHandler:^(NSURL *location,
                                                                                NSURLResponse *response,
                                                                                NSError *error) {
                                                                
                                                                NSData *imageData = [NSData dataWithContentsOfURL:location];
                                                                UIImage *image = [UIImage imageWithData:imageData];
                                                                dispatch_async(dispatch_get_main_queue(), ^{
                                                                    completion(image);
                                                                });
                                                            }];
    [task resume];
}

#pragma mark - Utilities
- (NSMutableDictionary *)defaultRequestParams
{
    NSMutableDictionary *params = [NSMutableDictionary new];
    
    params[@"format"] = @"json";
    params[@"api_key"] = LFMAPIKey;
    
    return params;
}

- (void)startTaskWithParams:(NSDictionary *)params
                 completion:(void(^)(id receivedObjects, NSError *error))completionBlock
{
    NSURL *url = [NSURL URLWithString:(NSString *)LastFMBaseURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *postBody = [params urlEncodedValue];
    request.HTTPBody = [postBody dataUsingEncoding:NSUTF8StringEncoding];;
    
    NSURLSessionDataTask *task = [self.session dataTaskWithRequest:request
                                                 completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                     if (error) {
                                                         completionBlock(nil, error);
                                                     } else {
                                                         error = nil;
                                                         id parced = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                                                         
                                                         if (!error) {
                                                             if ([parced isKindOfClass:[NSDictionary class]]) {
                                                                 
                                                                 NSInteger errorCode = [parced[@"error"] integerValue];
                                                                 if (errorCode > 0) {
                                                                     NSString *message = parced[@"message"];
                                                                     error = [NSError errorWithDomain:@"LFMAPIManager"
                                                                                                 code:errorCode
                                                                                             userInfo:@{NSLocalizedDescriptionKey : message}];
                                                                 }
                                                             }
                                                         }
                                                         completionBlock(parced, error);
                                                     }
                                                 }];
    [task resume];
}
@end
