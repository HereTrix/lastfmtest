//
//  LFMSettingsManager.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMSettingsManager.h"

static NSString const *LFMCountryStorageKey = @"LFMCountryStorageKey";

@interface LFMSettingsManager()

@property (nonatomic, strong) NSArray *countries;
@end

@implementation LFMSettingsManager

@synthesize selectedCountry = _selectedCountry;

+ (instancetype)sharedInstance
{
    static LFMSettingsManager *sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (LFMCountry *)selectedCountry
{
    if (!_selectedCountry) {
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSInteger index = [defaults integerForKey:(NSString *)LFMCountryStorageKey];
        NSArray *countries = [self availableCountries];
        _selectedCountry = countries[index];
    }
    return _selectedCountry;
}

- (void)setSelectedCountry:(LFMCountry *)selectedCountry
{
    _selectedCountry = selectedCountry;
    NSInteger index = [self.countries indexOfObject:selectedCountry];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setInteger:index forKey:(NSString *)LFMCountryStorageKey];
    [defaults synchronize];
}

- (NSArray *)availableCountries
{
    if (!self.countries) {
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"countries" ofType:@"plist"];
        NSArray *available = [NSArray arrayWithContentsOfFile:path];
        
        NSMutableArray *countriesArray = [NSMutableArray new];
        
        for (NSDictionary *countryObject in available) {
            
            LFMCountry *country = [LFMCountry createWithDictionary:countryObject];
            [countriesArray addObject:country];
        }
        
        self.countries = countriesArray;
    }
    
    return self.countries;
}
@end
