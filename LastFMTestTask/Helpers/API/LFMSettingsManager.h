//
//  LFMSettingsManager.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LFMCountry.h"

@interface LFMSettingsManager : NSObject

@property (nonatomic, strong) LFMCountry *selectedCountry;

+ (instancetype)sharedInstance;
- (NSArray *)availableCountries;
@end
