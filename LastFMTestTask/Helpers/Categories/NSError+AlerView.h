//
//  NSError+AlerView.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@class UIViewController;

@interface NSError (AlerView)

- (void)showErrorForController:(UIViewController *)controller;
@end
