//
//  NSDictionary+URLStringEncoding.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "NSDictionary+URLStringEncoding.h"

@implementation NSDictionary (URLStringEncoding)

- (NSString *)urlEncodedValue
{
    NSMutableString *encodedString = [NSMutableString new];
    
    for (NSString *key in self.keyEnumerator) {
        
        [encodedString appendFormat:@"&%@=%@", key, self[key]];
    }
    
    return [encodedString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];;
}
@end
