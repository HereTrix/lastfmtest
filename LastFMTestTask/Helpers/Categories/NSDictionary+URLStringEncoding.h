//
//  NSDictionary+URLStringEncoding.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (URLStringEncoding)

- (NSString *)urlEncodedValue;
@end
