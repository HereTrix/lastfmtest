//
//  NSError+AlerView.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "NSError+AlerView.h"

#import <UIKit/UIKit.h>

@implementation NSError (AlerView)

- (void)showErrorForController:(UIViewController *)controller
{
    UIAlertController *ac = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"ErrorTitle", nil)
                                                                message:self.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OkButton", nil)
                                                       style:UIAlertActionStyleDefault
                                                     handler:nil];
    [ac addAction:okAction];
    
    [controller presentViewController:ac animated:YES completion:nil];
}
@end
