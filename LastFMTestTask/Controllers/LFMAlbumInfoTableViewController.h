//
//  LFMAlbumInfoTableViewController.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LFMAlbum;

@interface LFMAlbumInfoTableViewController : UITableViewController

@property (nonatomic, strong) LFMAlbum *album;
@end
