//
//  LFMAlbumInfoTableViewController.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMAlbumInfoTableViewController.h"

#import "LFMAPIManager.h"
#import "LFMSong.h"
#import "LFMAlbum.h"

#import "NSError+AlerView.h"
#import "MBProgressHUD.h"

#import "LFMSongTableViewCell.h"

@interface LFMAlbumInfoTableViewController()

@property (nonatomic, strong) NSArray *songs;
@end

@implementation LFMAlbumInfoTableViewController

@synthesize album = _album;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = self.album.title;
    
    CGFloat width = CGRectGetWidth(self.view.frame);
    CGFloat height = 200;
    [self.album getCover:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView *tableHeader = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            tableHeader.image = image;
            self.tableView.tableHeaderView = tableHeader;
            [self.tableView reloadData];
        });
    }];
    
    [self loadTracks];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view processing
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.songs count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LFMSongTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SongCell"];
    
    LFMSong *song = self.songs[indexPath.row];
    cell.song = song;
    [cell setOrder:indexPath.row + 1];
    return cell;
}

- (void)loadTracks {
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    __weak typeof(self) weakRef = self;
    
    [[LFMAPIManager sharedInstance] getTracksForAlbum:self.album
                                           completion:^(NSArray *loadedObjects, NSError *error) {
                                               
                                               dispatch_async(dispatch_get_main_queue(), ^{
                                                   if (error) {
                                                       [error showErrorForController:weakRef];
                                                   } else {
                                                       weakRef.songs = loadedObjects;
                                                       [weakRef.tableView reloadData];
                                                   }
                                                   
                                                   [MBProgressHUD hideAllHUDsForView:weakRef.view
                                                                            animated:YES];
                                               });
                                           }];
}
@end
