//
//  LFMAlbumsCollectionViewController.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMAlbumsCollectionViewController.h"

#import "LFMAPIManager.h"

#import "NSError+AlerView.h"
#import "MBProgressHUD.h"

#import "LFMAlbumCollectionViewCell.h"
#import "LFMAlbumInfoTableViewController.h"

@interface LFMAlbumsCollectionViewController ()<UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) NSArray *albums;
@end

@implementation LFMAlbumsCollectionViewController

static NSString * const reuseIdentifier = @"AlbumCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.artist.name;
    [self loadDataForPage:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"ShowAlbumInfo"]) {
        LFMAlbumInfoTableViewController *destination = [segue destinationViewController];
        LFMAlbumCollectionViewCell *cell = sender;
        destination.album = cell.album;
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger columnCount = [UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad ? 3 : 2;
    CGFloat width  = CGRectGetWidth([UIScreen mainScreen].bounds) / columnCount;
    return CGSizeMake(width, 300);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.albums count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LFMAlbumCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    LFMAlbum *album = self.albums[indexPath.row];
    cell.album = album;
    
    [album getCover:^(UIImage *image) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [cell setCoverImage:image];
        });
    }];
    
    return cell;
}

#pragma mark - Data loading
- (void)loadDataForPage:(NSInteger)page
{
    __weak LFMAlbumsCollectionViewController *weakRef = self;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LFMAPIManager sharedInstance] getTopAlbumsForArtist:self.artist
                                               completion:^(NSArray *loadedObjects, NSError *error) {
                                                   
                                                   dispatch_async(dispatch_get_main_queue(), ^{
                                                       if (error) {
                                                           [error showErrorForController:weakRef];
                                                       } else {
                                                           weakRef.albums = loadedObjects;
                                                           [weakRef.collectionView reloadData];
                                                       }
                                                       [MBProgressHUD hideAllHUDsForView:weakRef.view
                                                                                animated:YES];
                                                   });
                                                   
                                               }];
}
@end
