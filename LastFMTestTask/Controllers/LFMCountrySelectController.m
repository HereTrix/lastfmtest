//
//  LFMCountrySelectController.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/13/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMCountrySelectController.h"

#import "LFMSettingsManager.h"

#import "LFMCountryCell.h"

@interface LFMCountrySelectController()

@end

@implementation LFMCountrySelectController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"SelectCountryTitle", nil);
}

- (IBAction)doneAction:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view processing
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[LFMSettingsManager sharedInstance].availableCountries count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LFMCountryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCell"];
    
    LFMCountry *country = [LFMSettingsManager sharedInstance].availableCountries[indexPath.row];
    
    cell.country = country;
    
    if (country == [LFMSettingsManager sharedInstance].selectedCountry) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    } else {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LFMSettingsManager *manager = [LFMSettingsManager sharedInstance];
    NSInteger prewRow = [manager.availableCountries indexOfObject:manager.selectedCountry];
    NSIndexPath *prewIndexPath = [NSIndexPath indexPathForRow:prewRow inSection:0];
    
    LFMCountry *country = manager.availableCountries[indexPath.row];
    manager.selectedCountry = country;
    
    NSArray *pathes = @[indexPath, prewIndexPath];
    
    [self.tableView reloadRowsAtIndexPaths:pathes withRowAnimation:UITableViewRowAnimationNone];
}
@end
