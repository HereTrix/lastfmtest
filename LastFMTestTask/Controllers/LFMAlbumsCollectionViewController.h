//
//  LFMAlbumsCollectionViewController.h
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "LFMArtist.h"

@interface LFMAlbumsCollectionViewController : UICollectionViewController

@property (nonatomic, strong) LFMArtist *artist;
@end
