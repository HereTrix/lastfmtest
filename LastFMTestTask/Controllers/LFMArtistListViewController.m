//
//  ArtistListViewController.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "LFMArtistListViewController.h"

#import "LFMAPIManager.h"
#import "LFMSettingsManager.h"

#import "NSError+AlerView.h"

#import "LFMArtist.h"

#import "MBProgressHUD.h"

#import "LFMArtistTableViewCell.h"
#import "LFMAlbumsCollectionViewController.h"

@interface LFMArtistListViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *artistListView;

@property (nonatomic, weak) IBOutlet UILabel *countryNameLabel;
@property (nonatomic, weak) IBOutlet UIImageView *countryFlagView;

@property (nonatomic, strong) LFMCountry *country;

@property (nonatomic, strong) NSArray *artists;
@end

@implementation LFMArtistListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"ArtistListTitle", nil);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.country = [LFMSettingsManager sharedInstance].selectedCountry;
    
    self.countryFlagView.image = self.country.icon;
    self.countryNameLabel.text = self.country.displayName;
    
    [self loadDataForPage:1];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView processing
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return  UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.artists count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    LFMArtistTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArtistListCell"];
    
    NSInteger index = indexPath.row;
    LFMArtist *artist = self.artists[index];
    cell.artist = artist;
    [cell setOrder:index+1];
    
    [[LFMAPIManager sharedInstance] getImage:artist.imageUrl
                                  completion:^(UIImage *image) {
                                      [cell setCoverImage:image];
                                  }];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"ShowArtistAlbums"]) {
        LFMAlbumsCollectionViewController *destination = segue.destinationViewController;
        LFMArtistTableViewCell *cell = sender;
        destination.artist = cell.artist;
    }
}

- (void)loadDataForPage:(NSInteger)page
{
    __weak LFMArtistListViewController *weakRef = self;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [[LFMAPIManager sharedInstance] getTopArtistsForCountry:self.country
                                                     atPage:1
                                                 completion:^(NSArray *loadedObjects, NSError *error) {
                                                     
                                                     dispatch_async(dispatch_get_main_queue(), ^{
                                                         if (error) {
                                                             [error showErrorForController:weakRef];
                                                         } else {
                                                             weakRef.artists = loadedObjects;
                                                             [weakRef.artistListView reloadData];
                                                         }
                                                         [MBProgressHUD hideAllHUDsForView:weakRef.view
                                                                                  animated:YES];
                                                     });
                                                 }];
}
@end
