//
//  AppDelegate.m
//  LastFMTestTask
//
//  Created by HereTrix on 11/12/15.
//  Copyright © 2015 HereTrix. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    NSUInteger cacheSizeMemory = 500*1024*1024;
    NSUInteger cacheSizeDisk = 500*1024*1024;
    [NSURLCache setSharedURLCache:[[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory
                                                                diskCapacity:cacheSizeDisk
                                                                    diskPath:nil]];
    return YES;
}
@end
